package com.qtask.restapi.com.qtask.restapi.controllers;


import com.qtask.restapi.com.qtask.restapi.models.Task;
import com.qtask.restapi.com.qtask.restapi.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TaskController {
    @Autowired
    TaskRepository taskRepository;

    @RequestMapping(method = RequestMethod.GET, value="/tasks")
    public Iterable<Task> task(){
        return taskRepository.findAll();
    }


    @RequestMapping(method = RequestMethod.POST, value="/tasks")
    public Task save(@RequestBody Task task){
        taskRepository.save(task);

        return task;
    }

    @RequestMapping(method = RequestMethod.GET, value="/tasks/{id}")
    public Task show(@PathVariable String id){
        return taskRepository.findOne(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value="/tasks/{id}")
    public Task update(@PathVariable String id, @RequestBody Task task){
        Task tempTask = taskRepository.findOne(id);
        if (task.getTaskName() != null)
            tempTask.setTaskName(task.getTaskName());
        if (task.getTeamName() != null)
            tempTask.setTeamName(task.getTeamName());
        if (task.getDod() != null)
            tempTask.setDod(task.getDod());
        if (task.getPriority() != null)
            tempTask.setTaskName(task.getPriority());
        taskRepository.save(tempTask);
        return tempTask;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/tasks/{id}")
    public String delete(@PathVariable String id){
        Task task   = taskRepository.findOne(id);
        taskRepository.delete(task);

        return "Task deleted";
    }
}
