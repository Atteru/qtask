package com.qtask.restapi.com.qtask.restapi.repositories;

import com.qtask.restapi.com.qtask.restapi.models.Task;
import org.springframework.data.repository.CrudRepository;

public interface TaskRepository extends CrudRepository<Task, String>{
    @Override
    Task findOne(String id);
    @Override
    void delete(Task deleted);
}
